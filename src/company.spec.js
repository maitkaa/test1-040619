const company = require('./company');


describe('company', () => {
    it ('company=3', () => {
        expect(company(3)).toMatchSnapshot();
    });
    it ('company=85', () => {
        expect(company(85)).toMatchSnapshot();
    });
	
    it('Throw id needs to be integer error', () => {
        expect(() => {
            company('string');
        }).toThrow('id needs to be integer');
    });
});	
